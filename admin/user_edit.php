<?php
	include('config.php');
	checkPermission(1);

	if(isset($_GET['id'])){
		$id=(int)$_GET['id'];
		
	} elseif(isset($_POST['id'])){
		$id=(int)$_POST['id'];
		
	} else{
		header("Location: user_list.php"); exit;
	}
	
	if(isset($_POST['action'])){
		if($_POST['action']=="edit"){
			$first_name=mysql_real_escape_string($_POST['first_name']);
			$last_name=mysql_real_escape_string($_POST['last_name']);
			$email=mysql_real_escape_string($_POST['email']);
			$phone=mysql_real_escape_string($_POST['phone']);
			$country=mysql_real_escape_string($_POST['country']);
			$city=mysql_real_escape_string($_POST['city']);
			$is_contact=intval($_POST['is_contact']);

			$result = mysql_query("SELECT COUNT(id) AS count FROM ".$db_pref."users WHERE email='".$email."' AND id != ".$id);
			$row = mysql_fetch_assoc($result);
			$count = $row['count'];
				
			if($count>0){
				 $messege='Данный e-mail уже зарегистрирован!';
			}else{
				$result = mysql_query("SELECT COUNT(id) AS count FROM ".$db_pref."users WHERE phone='".$phone."' AND id != ".$id);
				$row = mysql_fetch_assoc($result);
				$count = $row['count'];
					
				if($count>0){
					 $messege='Данный номер телефона уже зарегистрирован!';
				}else{
					$coords = getCoords($country.' '.$city);
				
				
					mysql_query("update ".$db_pref."users set 
						first_name='".$first_name."', 
						last_name='".$last_name."', 
						email='".$email."', 
						phone='".$phone."', 
								longitude='".$coords['lng']."', 
								latitude='".$coords['lat']."', 
						country='".$country."', 
						is_contact='".$is_contact."', 
						city='".$city."'
						where id=".$id) or die(mysql_error());
						
					if($is_contact=='0'){
						$result = @mysql_query("DELETE FROM ".$db_pref."map WHERE user_id=".$id,$connection) or die("er");
					}else{
						$r = mysql_query("SELECT COUNT(id) AS count FROM ".$db_pref."map WHERE user_id = ".$id);
						$row = mysql_fetch_assoc($r);
						
						$c_position=mysql_real_escape_string($_POST['c_position']);
						$c_name=mysql_real_escape_string($_POST['c_name']);
						$c_country=mysql_real_escape_string($_POST['c_country']);
						$c_city=mysql_real_escape_string($_POST['c_city']);
						$c_phone=mysql_real_escape_string($_POST['c_phone']);
						$c_mob_phone=mysql_real_escape_string($_POST['c_mob_phone']);
						$c_fax=mysql_real_escape_string($_POST['c_fax']);
						$c_email=mysql_real_escape_string($_POST['c_email']);

						
						$coords = getCoords($c_country.' '.$c_city);
						
						
						if($row['count'] == 1){

							mysql_query("update ".$db_pref."map set 
								name='".$c_name."',
											longitude='".$coords['lng']."', 
											latitude='".$coords['lat']."', 
								position='".$c_position."', phone='".$c_phone."', mob_phone='".$c_mob_phone."', fax='".$c_fax."', email='".$c_email."', country='".$c_country."', city='".$c_city."'
								where user_id=".$id) or die(mysql_error());		

						}else{
							mysql_query("INSERT INTO ".$db_pref."map(id, name, position, phone, mob_phone, fax, email, country, city, user_id, longitude, latitude) VALUES ('0', '".$c_name."',  '".$c_position."', '".$c_phone."', '".$c_mob_phone."', '".$c_fax."', '".$c_email."', '".$c_country."', '".$c_city."', '".$id."', '".$coords['lng']."', '".$coords['lat']."')") or die (mysql_error());
						
						}
					
					}
						
						
				
					$messege="Информация сохранена";
				}

			}
			
			
			
			
			
			

		}
	}
	
	if($_GET['act']=="delete"){
		$result = @mysql_query("DELETE FROM ".$db_pref."users WHERE id=".$id,$connection) or die("er");
		$result = @mysql_query("DELETE FROM ".$db_pref."register_answers WHERE id_user=".$id,$connection) or die("er");
		$result = @mysql_query("DELETE FROM ".$db_pref."map WHERE user_id=".$id,$connection) or die("er");
		header("Location: user_list.php"); exit;
	}

	
	$active_page='users';
	$title_page='Пользователи / Просмотр пользователя';

	
	if(is_numeric($id)){
		$sql_1= "SELECT * FROM ".$db_pref."users WHERE id=".$id;
		$result_1 = @mysql_query($sql_1);
		$row_1 = mysql_fetch_array($result_1);		
		
		if(!isset($row_1['id'])){
			header("Location: user_list.php");
			exit;
		}else{
			
			$f_name=stripslashes($row_1['first_name']);
			$l_name=stripslashes($row_1['last_name']);
			$email=stripslashes($row_1['email']);
			$phone=stripslashes($row_1['phone']);
			$country=stripslashes($row_1['country']);
			$city=stripslashes($row_1['city']);
			$user_ip=stripslashes($row_1['user_ip']);
			$user_agent=stripslashes($row_1['user_agent']);
			$date=stripslashes($row_1['date']);
			$time=stripslashes($row_1['time']);
			$is_contact=$row_1['is_contact'];

			$map = @mysql_query("SELECT * FROM ".$db_pref."map WHERE user_id=".$id);
			$m = mysql_fetch_array($map);		
			
			$c_position = $m['position'];
			$c_name = $m['name'];
			$c_country = $m['country'];
			$c_city = $m['city'];
			$c_phone = $m['phone'];
			$c_mob_phone = $m['mob_phone'];
			$c_fax = $m['fax'];
			$c_email = $m['email'];

		}

	
	}else{
		header("Location: user_list.php");
		exit;
	}

	include('header.php');
	
	
?>


     <?php if(isset($messege)){echo'<div class="alert alert-info">'.$messege.'</div>';}?>
<form method='post' action='user_edit.php?id=<?php echo $id?>'>
		<table class="table" >

				<tr>
					<td>Имя</td>
					<td><input type="text" name="first_name" class="form-control"  value="<?php echo $f_name?>"/></td>                    
				</tr>
				<tr>
					<td>Фамилия</td>
					<td><input type="text" name="last_name" class="form-control"  value="<?php echo $l_name?>"/></td>                    
				</tr>
                <tr>
					<td>Страна</td>
					<td><input type="text" name="country" class="form-control"  value="<?php echo $country?>"/></td>                    
				</tr>
                <tr>
					<td>Город</td>
					<td><input type="text" name="city" class="form-control"  value="<?php echo $city?>"/></td>                    
				</tr>
                <tr>
					<td>E-mail</td>
					<td><input type="text" name="email" class="form-control"  value="<?php echo $email?>"/></td>                    
				</tr>
                <tr>
					<td>Телефон</td>
					<td><input type="text" name="phone" class="form-control"  value="<?php echo $phone?>"/></td>                    
				</tr>
				<tr>
					<td>Ip Адрес</td>
					<td><input type="text" disabled name="user_ip" class="form-control"  value="<?php echo $user_ip?>"/></td>                    
				</tr>
				<tr>
					<td>Устройство</td>
					<td><input type="text" disabled name="user_agent" class="form-control"  value="<?php echo $user_agent?>"/></td>                    
				</tr>
				<tr>
					<td>Дата регистрации</td>
					<td><input type="text" disabled name="date" class="form-control"  value="<?php echo $date?>"/></td>                    
				</tr>
				<tr>
					<td>Время регистрации</td>
					<td><input type="text" disabled name="time" class="form-control"  value="<?php echo $time?>"/></td>                    
				</tr>
				
                <tr>
					<td>Контакт на карате</td>
					<td>
						<select name="is_contact" class="form-control" id="is_contact">
							<option value="0" <?php echo ($is_contact=='0')?' selected="selected"':''; ?>>Нет</option>
							<option value="1" <?php echo ($is_contact=='1')?' selected="selected"':''; ?>>Да</option>
						</select>
					</td>                    
				</tr>

				<tr>
                	<td></td>
					<td>

						<input type='hidden' name='action' value='edit' />
                        <a href="user_edit.php?id=<?php echo $id?>&act=delete" class="btn btn-danger pull-left"  onclick='javascript:return confirm("Вы уверены?")'>Удалить</a>
                        <input type='submit' value='Сохранить' class='btn btn-primary pull-right' />
						 <input type='button' value='Отмена' class='btn btn-default pull-right' onclick="location.href='user_list.php'" style="margin-right:20px;" />

						
					</td>
				</tr>
			</table>
			
			<div id="contact_block">
			
			
			<table class="table" >
				<tr>
					<td colspan="2">Контакт на карате</td></tr>
				<tr>
					<td>Должность</td>
					<td><input type="text" name="c_position" class="form-control" value="<?php echo $c_position ?>"/></td>
				</tr>
				<tr>
					<td>Имя</td>
					<td><input type="text" name="c_name" class="form-control" value="<?php echo $c_name?>"/></td>
				</tr>
                <tr>
					<td>Страна</td>
					<td><input type="text" name="c_country" class="form-control" value="<?php echo $c_country?>"/></td>
				</tr>
                <tr>
					<td>Город</td>
					<td><input type="text" name="c_city" class="form-control" value="<?php echo $c_city?>"/></td>
				</tr>
                <tr>
					<td>Телефон</td>
					<td><input type="text" name="c_phone" class="form-control" value="<?php echo $c_phone?>"/></td>
				</tr>
                
                <tr>
					<td>Моб. телефон</td>
					<td><input type="text" name="c_mob_phone" class="form-control" value="<?php echo $c_mob_phone?>"/></td>
				</tr>
                <tr>
					<td>Факс</td>
					<td><input type="text" name="c_fax" class="form-control" value="<?php echo $c_fax?>"/></td>
				</tr>
				<tr>
					<td>E-mail</td>
					<td><input type="text" name="c_email" class="form-control" value="<?php echo $c_email?>"/></td>
				</tr>


			</table>
			
			</div>
            </form>
            </div>
            </div>
<div class="panel panel-info">
<div class="panel-heading">Ответы на вопросы</div>
  <div class="panel-body">
  
  <table class="table" id="table">
  <thead>
     <tr>
         <th>Вопрос</th>
         <th>Ответ</th>                          
     </tr>
                       
	</thead>
    <tbody>
	<?php
    
    
        $query_limited = "select * from ".$db_pref."register_answers where id_user='".$id."'  ORDER BY id"; 
        $final_result = mysql_query($query_limited);
        while ($row = mysql_fetch_assoc($final_result)) {
            
            $q = "select * from ".$db_pref."register_questions where id='".$row['id_question']."'";	
            $f = mysql_query($q);
            $r = mysql_fetch_assoc($f);
            
            
            
                echo"<tr>
                   
                    <td>".$r['question']."</td>
                    <td>".$row['answer']."</td>                    
                </tr>";
                
            
        }
    ?>
    </tbody>
	</table>

<script>
function conCH(){
	var is_contact = $("#is_contact").val();
	
	if(is_contact == 1){
		$("#contact_block").css({"display": "block"});
	}else{
		$("#contact_block").css({"display": "none"});
	}

}

$(document).ready(function(){
	conCH();

	$("#is_contact").on("change",function() {
		conCH();
	});
});




</script>

<?php include('footer.php'); ?>