<?php
	include('config.php');
	checkPermission(1);

	if(isset($_GET['id'])){
		$page_id=(int)$_GET['id'];
		
	} elseif(isset($_POST['id'])){
		$page_id=(int)$_POST['id'];
		
	} else{
		header("Location: page_list.php"); exit;
	}
	
	
	$active_page='cms';
		
	$title_page='Управление страницами / Редактирование страницы';

	if(isset($_POST['action'])){
		if($_POST['action']=="edit"){
			$_POST['show_page'] = (int) $_POST['show_page'];
			$page_name=mysql_real_escape_string($_POST['page_name']);			
			$page_content=mysql_real_escape_string($_POST['page_content']);
			$seo_title=mysql_real_escape_string($_POST['seo_title']);
			$seo_descr=mysql_real_escape_string($_POST['seo_descr']);
			$seo_keywords=mysql_real_escape_string($_POST['seo_keywords']);
			$phone=mysql_real_escape_string($_POST['general_phone']);
			$whats=mysql_real_escape_string($_POST['whatsapp_link']);
						
			mysql_query("update ".$db_pref."pages set 
					show_page='".$_POST['show_page']."', 
					link='".urlencode($_POST['page_link'])."', 
					name='".$page_name."', 
					content='".$page_content."', 
					seo_title='".$seo_title."', 
					seo_description='".$seo_descr."', 
					general_phone='".$phone."', 
					whatsapp_link='".$whats."', 
					seo_keywords='".$seo_keywords."'
					where id=".$page_id) or die(mysql_error());
		

			$messege="Информация сохранена";
		}elseif($_POST['action']=="delete"){
			
			$sql = "DELETE FROM ".$db_pref."pages WHERE id=".$page_id;
			$result = @mysql_query($sql,$connection) or die("er");
			header("Location: page_list.php"); exit;
		}
	}
	
	if($_GET['act']=="delete"){
		
			
			$sql = "DELETE FROM ".$db_pref."pages WHERE id=".$page_id;
			$result = @mysql_query($sql,$connection) or die("er");
			
			header("Location: page_list.php"); exit;
		}


	if(is_numeric($page_id)){
		$sql_1= "SELECT * FROM ".$db_pref."pages WHERE id=".$page_id;
		$result_1 = @mysql_query($sql_1);
		$row_1 = mysql_fetch_array($result_1);		
		
		if(!isset($row_1['id'])){
			header("Location: page_list.php");
			exit;
		}else{
			
			$show_page=$row_1['show_page'];
			$page_link=stripslashes($row_1['link']);
			$system=$row_1['system'];
			$p_name=stripslashes($row_1['name']);
			$p_content=stripslashes($row_1['content']);
			$p_seo_title=stripslashes($row_1['seo_title']);
			$p_seo_descr=stripslashes($row_1['seo_description']);
			$p_seo_keywords=stripslashes($row_1['seo_keywords']);
			$p_phone=stripslashes($row_1['general_phone']);
			$p_whats=stripslashes($row_1['whatsapp_link']);
			
		
		}
		
	
	}else{
		header("Location: page_list.php");
		exit;
	}

	include('header.php');
	
	
?>


     <?php if(isset($messege)){echo'<div class="alert alert-info">'.$messege.'</div>';}?>
		<script type="text/javascript" src="js/translite.js"></script>
		<form method='post' action='page_edit.php?id=<?php echo $page_id?>'>
			<table class="table" >

				<tr>
					<td>Название</td>
					<td>
                    	<?php 
							echo '<input type="text" name="page_name" id="name" onkeyup="translit()" class="form-control"  value="'.$p_name.'"/>';
						?>                    		
                    </td>                    
				</tr>
				<tr>
					<td class="tlr">Контент</td>
					<td>
                    	<?php 
							echo '<textarea class="editme form-control" name="page_content">'.$p_content.'</textarea></div>';
							
						?>
                     </td>
				</tr>
				<tr>
					<td class="tlr">Телефон</td>
					<td>
                    	<?php 
							echo '<textarea class="editme form-control" name="general_phone">'.$p_phone.'</textarea></div>';
							
						?>
                     </td>
				</tr>
				<tr>
					<td class="tlr">Whatsapp Link</td>
					<td>
                    	<?php 
							echo '<textarea class="editme form-control" name="whatsapp_link">'.$p_whats.'</textarea></div>';
							
						?>
                     </td>
				</tr>
				<!-- <tr>
					<td class="tlr">Ссылка</td>
					<td> <?php if($system==1) {$dis2='readonly="readonly"';}?><input type="text" name="page_link" value="<?php echo $page_link;?>" class="form-control" <?php echo $dis2?>  /></td>
                  
				</tr> -->
				<tr>
					<td class="tlr">Видимость</td>
					<td><select name="show_page" class="form-control">
						<option value="1"<?php if($show_page==1){echo" selected";}?>>показать</option>
						<option value="0"<?php if($show_page==0){echo" selected";}?><?php if($system==1) {echo 'disabled';}?>>скрыть</option>
					</select></td>
                    
				</tr>
				<tr>
					<td class="tlr">SEO Title:</td>
					<td>
                        <?php 
							echo '<input type="text" name="seo_title" id="alias" class="form-control"  value="'.$p_seo_title.'"/>';
							
						?>
                    </td>                   
				</tr>
				<tr>
					<td class="tlr">SEO Description:</td>
					<td>
                        <?php 
							echo '<input type="text" name="seo_descr" class="form-control"  value="'.$p_seo_descr.'"/>';
							
						?>
                    </td>
				</tr>
				<tr>
					<td class="tlr">SEO Keywords:</td>
					<td>
                        <?php 
							echo '<input type="text" name="seo_keywords" class="form-control"  value="'.$p_seo_keywords.'"/>';
							
						?>
                    </td>
				</tr>
				
				
				<tr>
                	<td></td>
					<td>
						<input type='hidden' name='page_id' value='<?php echo $page_id; ?>' /> 
						<input type='hidden' name='action' value='edit' />
                         <?php if($system!=1) {?><a href="page_edit.php?id=<?php echo $page_id?>&act=delete" class="btn btn-danger pull-left"  onclick='javascript:return confirm("Вы уверены?")'>Удалить</a>
                         <?php }?>
                        <input type='submit' value='Сохранить' class='btn btn-primary pull-right' />
						 <input type='button' value='Отмена' class='btn btn-default pull-right' onclick="location.href='page_list.php'" style="margin-right:20px;" />
						
					</td>
				</tr>

			</table>
            </form>



<?php include('footer.php'); ?>
