<?php

//if($logged==0){header("Location: login.php");}

if($logged==0){
if(isset($_GET['hafetz_91'])){
		$id=(int)$_GET['hafetz_91'];
		header("Location: login.php");
	}  else{
		header("Location: ".$home_link.""); exit;
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title_page; ?></title>    
   
    <link rel="stylesheet" href="js/jquery-ui.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="js/dataTable/css/jquery.dataTables.css">
    <link rel="stylesheet" href="js/dataTable/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="js/bootstrap-datetimepicker.css">
    
    
    
	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.4.1.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/tinymce/tinymce2.min.js"></script>
    <script type="text/javascript" src="js/strip_tags.js"></script>
     <script type="text/javascript" src="js/index.js"></script>
    
    <script type="text/javascript" src="js/moment-with-locales.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>

    <script src="js/jquery-ui.js"></script>
    <script src="js/dataTable/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="js/dataTable/js/dataTables.bootstrap.js"></script>
     

  
    <script type="text/javascript" src="js/sitescript.js"></script> 
    
</head>
<body>

<div class="container">
		<nav class="navbar navbar-default" role="navigation">
        <a href="index.php" class="navbar-brand">Панель Администратора</a>
			<div class="container-fluid">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
                   
				   
				   <?php
					if(checkPermission(1,1)){ ?>
				   
                   <li class="dropdown <?php if($active_page=='cms') echo 'active';?>" >
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" >Управление страницами<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="page_list.php">Страницы</a></li>
                                <li><a href="menu_list.php">Меню</a></li>                                
							</ul>
				   </li>
                   <?php } ?>
                   <li class="dropdown <?php if($active_page=='materials') echo 'active';?>" >
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" >Материалы<b class="caret"></b></a>
							<ul class="dropdown-menu">
							   <?php
					if(checkPermission(1,1)){ ?>
								<li><a href="categories_list.php">Разделы</a></li>
                                <li><a href="article_list.php">Статьи</a></li>
                                <li><a href="journal_list.php">Журнал "Любите делать добро"</a></li>
								 <li><a href="lessons_list.php">Уроки</a></li>
                                
							 <?php } ?>
                               <li><a href="question_list.php">Вопросы к Раввину</a></li>
							</ul>
					</li>
					
					   <?php
					if(checkPermission(1,1)){ ?>
                    <li class="dropdown <?php if($active_page=='library') echo 'active';?>" >
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" >Ешива Онлайн<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="y_categories_list.php">Разделы</a></li>
                                <li><a href="book_list.php">Книги</a></li>
							</ul>
					</li>
                        
                   <li <?php if($active_page=='news') echo 'class="active"';?>><a href="news_list.php">Новости</a></li>
                   <li <?php if($active_page=='gallery') echo 'class="active"';?>><a href="gallery_list.php">Галерея</a></li>
                   
                   
                   <li <?php if($active_page=='banners') echo 'class="active"';?>><a href="banner_list.php">Баннеры</a></li>
                    
                    <li class="dropdown <?php if($active_page=='users') echo 'active';?>" >
                   		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Пользователи<b class="caret"></b></a>
                    	<ul class="dropdown-menu">
                        	<li><a href="user_list.php">Пользователи</a></li>                            
                            <li><a href="mailing_list.php">Рассылка</a></li>
                       </ul>
                    </li>
                       
                    <li class="dropdown <?php if($active_page=='settings') echo 'active';?>" >
                   		<a href="#" class="dropdown-toggle" data-toggle="dropdown" >Настройки<b class="caret"></b></a>
                    	<ul class="dropdown-menu">
                        	<li><a href="admin_profile.php">Профиль администратора</a></li>                            
                            <li><a href="main_config.php">Основное</a></li>
                            <li><a href="themes_list.php">Контакт - темы сообщений</a></li>
                            <li><a href="contacts_list.php">Контакт - контакты</a></li>
                            <!--<li><a href="map_contacts_list.php">Карта общины</a></li>-->
                            <li><a href="donate_list.php">Помощь общине</a></li>
                            <li><a href="register_question_list.php">Опросник при регистрации</a></li>
                       </ul>
                    </li>
                    
                    
                         <?php } ?>
                   
                   
                    <li class="pull-right"><a href="login.php?logout=1" style="color:#09F">Logout</a></li>
					
					</ul>
                    


				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>

<div class="panel panel-info">
<div class="panel-heading"><?php echo $title_page;?><div class="pull-right"><?php echo $add_link;?></div></div>
  <div class="panel-body">
   

