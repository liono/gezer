<?php
	include('config.php');
	checkPermission(1);
	
	$active_page='cms';

	$title_page='Управление страницами / Список страниц';
	$add_link='<a href="page_add.php" class="title_add"><span class="glyphicon glyphicon-plus"></span> Добавить новую страницу</a>';

	include('header.php');
?>
  <table class="table" id="table">
  <thead>
                   		<tr>
                        	<th>№</th>
                            <th>Название</th>
                            <th>Видимость</th>
                            <th></th>                           
                        </tr>
                       
	</thead>
    <tbody>
<?php


	$query_limited = "select * from ".$db_pref."pages ORDER BY id desc";
	$final_result = mysql_query($query_limited);
	while ($row = mysql_fetch_assoc($final_result)) {
		
		if($row['show_page']==1){$shown='<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>';}
		else{$shown='<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>';}
			
			echo"<tr>
				<td>".$row['id']."</td>
				<td>".stripslashes($row['name'])."</td>
				<td>".$shown."</td>
				<td><a href='page_edit.php?id=".$row['id']."' ><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span></a></td>
			</tr>";
			
		
	}
?>
</tbody>
	</table>
<script>
$(document).ready(function() {
    $('#table').DataTable();
} );
 

</script>


<?php include('footer.php'); ?>