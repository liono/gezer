<?php
	include('config.php');
	checkPermission(1);
	
	$active_page='library';
	$title_page='Ешива онлайн / Порядок отображения категорий';

	include('header.php');
?>

  

<div class="panel panel-success">
<div class="panel-heading">Порядок отображения разделов</div>
  <div class="panel-body">
	<div class="alert alert-info">Вы можете изменить порядок отображения разделов путем перетаскивания пункта в нужное место. Порядок пунктов будет сохранен автоматически.</div>
 
<?php


echo "<div class='block'><ul id='sortable'  class='list-group'>";
$sql_1= "SELECT * FROM ".$db_pref."categories where status=1 and type=1  ORDER BY position";
$result_1 = @mysql_query($sql_1) or die('err');
while ($row = mysql_fetch_array($result_1)) {
	echo "<li id='{$row['id']}' class='ui-state-default list-group-item'>{$row['name']}</li>";
}
echo "</ul></div>";			
				
?>
</div>
</div>    


<script>

$(document).ready(function(){
	$('#sortable').sortable({
		axis: 'y',
		opacity: 0.5,
		placeholder: 'ui-state-default',
		containment: '.block',
		stop: function(){
			var arr = $('#sortable').sortable("toArray");
			//alert(arr);
			$.ajax({
				url: 'php/sort_category.php',
				type: 'POST',
				data: {masiv:arr},
				error: function(){
					$('#res').text("Ошибка!");
				},
				success: function(){
					$('#res').show().text("Сохранено!").fadeOut(1000);
				}
			});
		}
	});
	
	
});

</script>


<?php include('footer.php'); ?>