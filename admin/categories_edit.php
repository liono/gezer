<?php
	include('config.php');
	checkPermission(1);

	if(isset($_GET['id'])){
		$page_id=(int)$_GET['id'];
		
	} elseif(isset($_POST['id'])){
		$page_id=(int)$_POST['id'];
		
	} else{
		header("Location: categories_list.php"); exit;
	}
	
	
	$active_page='materials';
		
	$title_page='Материалы / Редактирование раздела';

	if(isset($_POST['action'])){
		if($_POST['action']=="edit"){
			$name=mysql_real_escape_string($_POST['name']);						
			$seo_title=mysql_real_escape_string($_POST['seo_title']);
			$seo_descr=mysql_real_escape_string($_POST['seo_descr']);
			$seo_keywords=mysql_real_escape_string($_POST['seo_keywords']);
			$block_password=mysql_real_escape_string($_POST['block_password']);
						
			mysql_query("update ".$db_pref."categories set 
					status='".$_POST['status']."', 	
					status_index='".$_POST['status_index']."', 			
					name='".$name."', 
					seo_title='".$seo_title."', 
					seo_description='".$seo_descr."', 
					seo_keywords='".$seo_keywords."',
					block='".$_POST['block']."', 
					password='".$block_password."'
					where id=".$page_id) or die(mysql_error());
		

			$messege="Информация сохранена";
		}elseif($_POST['action']=="delete"){
			
			$sql = "DELETE FROM ".$db_pref."categories WHERE id=".$page_id;
			$result = @mysql_query($sql,$connection) or die("er");
			header("Location: categories_list.php"); exit;
		}
	}
	
	if($_GET['act']=="delete"){
			
			$sql = "DELETE FROM ".$db_pref."categories WHERE id=".$page_id;
			$result = @mysql_query($sql,$connection) or die("er");
			header("Location: categories_list.php"); exit;
		}


	if(is_numeric($page_id)){
		$sql_1= "SELECT * FROM ".$db_pref."categories WHERE id=".$page_id;
		$result_1 = @mysql_query($sql_1);
		$row_1 = mysql_fetch_array($result_1);		
		
		if(!isset($row_1['id'])){
			header("Location: categories_list.php");
			exit;
		}else{			
			$status=$row_1['status'];	
			$system=$row_1['system'];	
			$status_index=$row_1['status_index'];			
			$p_name=stripslashes($row_1['name']);
			$p_seo_title=stripslashes($row_1['seo_title']);
			$p_seo_descr=stripslashes($row_1['seo_description']);
			$p_seo_keywords=stripslashes($row_1['seo_keywords']);
			$block=$row_1['block'];
			$block_password=stripslashes($row_1['password']);
		}
		
	
	}else{
		header("Location: categories_list.php");
		exit;
	}

	include('header.php');
	
	
?>


     <?php if(isset($messege)){echo'<div class="alert alert-info">'.$messege.'</div>';}?>
		<script type="text/javascript" src="js/translite.js"></script>
		<form method='post' action='categories_edit.php?id=<?php echo $page_id?>'>
			<table class="table" >

				<tr>
					<td>Название</td>
					<td>
                    	<?php 
							echo '<input type="text" name="name" id="name" onkeyup="translite();" class="form-control"  value="'.$p_name.'"/>';
						?>                    		
                    </td>                    
				</tr>	
                
                <tr>
					<td>Блокировка</td>
					<td><select name="block" class="form-control">
                    	<option value="0"<?php if($block==0){echo" selected";}?>>нет</option>
						<option value="1"<?php if($block==1){echo" selected";}?>>да</option>						
					</select></td>
				</tr>	
                <tr>
					<td>Пароль блокировки</td>
					<td><input type="text" name="block_password" class="form-control" value="<?php echo $block_password?>"/></td>
				</tr>			
				
                
                <tr>
					<td class="tlr">Отображение в меню</td>
					<td><select name="status" class="form-control">
						<option value="1"<?php if($status==1){echo" selected";}?>>показать</option>
						<option value="0"<?php if($status==0){echo" selected";}?>>скрыть</option>
					</select></td> 
				</tr>
                <tr>
					<td class="tlr">Отображение на главной</td>
					<td><select name="status_index" class="form-control">
						<option value="1"<?php if($status_index==1){echo" selected";}?>>показать</option>
						<option value="0"<?php if($status_index==0){echo" selected";}?>>скрыть</option>
					</select></td> 
				</tr>
                
				<tr>
					<td class="tlr">SEO Title:</td>
					<td>
                        <?php 
							echo '<input type="text" name="seo_title" id="alias" class="form-control"  value="'.$p_seo_title.'"/>';
							
						?>
                    </td>                   
				</tr>
				<tr>
					<td class="tlr">SEO Description:</td>
					<td>
                        <?php 
							echo '<input type="text" name="seo_descr" class="form-control"  value="'.$p_seo_descr.'"/>';
							
						?>
                    </td>
				</tr>
				<tr>
					<td class="tlr">SEO Keywords:</td>
					<td>
                        <?php 
							echo '<input type="text" name="seo_keywords" class="form-control"  value="'.$p_seo_keywords.'"/>';
							
						?>
                    </td>
				</tr>
				
				
				<tr>
                	<td></td>
					<td>
						<input type='hidden' name='page_id' value='<?php echo $page_id; ?>' /> 
						<input type='hidden' name='action' value='edit' />
                         <?php if($system!=1) {?><a href="categories_edit.php?id=<?php echo $page_id?>&act=delete" class="btn btn-danger pull-left"  onclick='javascript:return confirm("Вы уверены?")'>Удалить</a>
                         <?php }?>
                        <input type='submit' value='Сохранить' class='btn btn-primary pull-right' />
						 <input type='button' value='Отмена' class='btn btn-default pull-right' onclick="location.href='categories_list.php'" style="margin-right:20px;" />
						
					</td>
				</tr>

			</table>
            </form>



<?php include('footer.php'); ?>