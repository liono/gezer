			
$(function() {
				
		$('#date_range').on('click', function() {
			$('#show_datepikker').toggle();
		});
		$('a.cancel').on('click', function() {
			$('#show_datepikker').toggle();
		});
					
		/*$('#pers_area').on('click', function() {
			$('#show_pers_area').toggle('normal');
		});*/
});
/*(function($) {
     setTimeout(function() {  
			$('select').styler({selectPlaceholder: '',filePlaceholder: 'No file selected',fileBrowse: 'Choose',});  
	}, 10)
})(jQuery);*/


$(function () {
    var tabContainers = $('div.tabs > div'); // получаем массив контейнеров
    tabContainers.hide().filter(':first').show(); // прячем все, кроме первого
    // далее обрабатывается клик по вкладке
    $('div.tabs ul.tabNavigation a').click(function () {
        tabContainers.hide(); // прячем все табы
        tabContainers.filter(this.hash).show(); // показываем содержимое текущего
        $('div.tabs ul.tabNavigation a').removeClass('selected'); // у всех убираем класс 'selected'
        $(this).addClass('selected'); // текушей вкладке добавляем класс 'selected'
        return false;
    }).filter(':first').click();
});

$(function () {
    var tabContainers = $('div.tabs2 > div'); // получаем массив контейнеров
   // tabContainers.hide().filter(':first').show(); // прячем все, кроме первого
    // далее обрабатывается клик по вкладке
    $('div.tabs2 ul.tabNavigation2 a').click(function () {
        tabContainers.hide(); // прячем все табы
        tabContainers.filter(this.hash).show(); // показываем содержимое текущего
        $('div.tabs2 ul.tabNavigation2 a').removeClass('selected'); // у всех убираем класс 'selected'
        $(this).addClass('selected'); // текушей вкладке добавляем класс 'selected'
        return false;
    });
});


 $(function() {
    $( "#from" ).datepicker({
      
	  dateFormat: "yy-mm-dd",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      
	  dateFormat: "yy-mm-dd",
      changeMonth: true,
      numberOfMonths: 1,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
	
	$( ".date_delivery" ).datepicker({
      
	  dateFormat: "yy-mm-dd",
	  minDate: "0",
      changeMonth: true,
      numberOfMonths: 1      
    });
	
  });
  
  
 /* var elems = document.querySelectorAll(".inp[data-lang]");
      document.getElementById("switch_lang").onchange = function () {
        var val = this.value;
        for (var el, i = 0; i < elems.length; i++) {
          el = elems[i];
          el.className = el.getAttribute('data-lang') == val ? "show" : "";
        }
      };*/

function change_lang(elem){
	 var val = elem.value;
	 tagList = document.getElementsByName('switch_lang'); 
		for (var i = 0; i < tagList.length; i++) 
		tagList[i].value = val;
		//alert(tagList[i].nodeName);
		
	 
	 
	 //alert(val);
	 var elems = document.querySelectorAll(".lang_sw[data-lang]");
	  for (var el, i = 0; i < elems.length; i++) {
          el = elems[i];
          //el.className = el.getAttribute('data-lang') == val ? "inp show" : "inp";
		  
		  el.style.display = el.getAttribute('data-lang') == val ? "block" : "none";;
        }
   //if(elem.value == 1)
      //document.getElementById('hidden_div').style.display = "block";
}


tinymce.init({
    // General options
    selector:'textarea.editme', 
	menubar: 'table tools',
	statusbar: false,
	min_height: 200,
	plugins : "link, code, paste, jbimages, media, image",  
	oninit : "setPlainText",
	toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages media",
	media_dimensions: true,
	style_formats: [
    { title: 'Headers', items: [
      { title: 'h1', block: 'h1' },
      { title: 'h2', block: 'h2' },
      { title: 'h3', block: 'h3' },
      { title: 'h4', block: 'h4' }
    ] },
  ],
  relative_urls : false,
remove_script_host : false,
convert_urls : true,
  
    theme_advanced_statusbar_location : 'bottom',
	theme_advanced_resizing : true,
	//convert_urls : 0,
    theme_advanced_resize_horizontal : false        ,
	//relative_urls: false,
	force_br_newlines : false,
    force_p_newlines : true,
    forced_root_block : '', // Needed for 3.x
paste_use_dialog : false,
paste_auto_cleanup_on_paste : true,
paste_convert_headers_to_strong : false,
paste_strip_class_attributes : "all",
paste_remove_spans : true,
paste_remove_styles : true,
paste_retain_style_properties : "",
    paste_preprocess : function(pl, o) {
        o.content = strip_tags(o.content, '<p><a>');
    }


   /* paste_remove_styles: true,
    paste_remove_spans: true,
    paste_strip_class_attributes: 'all',
    paste_text_sticky : true,
    paste_block_drop: true,
    paste_preprocess : function(pl, o) {
        console.log(o.content);
        o.content = strip_tags(o.content, '<p><a>');
        console.log(o.content);
    },*/
	/* external_filemanager_path:"/admin/filemanager/",
   filemanager_title:"Responsive Filemanager" ,
   external_plugins: { "filemanager" : "/admin/filemanager/plugin.min.js"}*/
});
