<?php
        date_default_timezone_set('America/Los_Angeles');
	include('config.php');
	checkPermission(1);

	$active_page='materials';
	$title_page='Материалы / Добавление статьи';

	if(isset($_POST['action']) and $_POST['action']=="add"){
		
		$page_name=mysql_real_escape_string($_POST['page_name']);			
		$page_content=mysql_real_escape_string($_POST['page_content']);
		$seo_title=mysql_real_escape_string($_POST['seo_title']);
		$fb_change=mysql_real_escape_string($_POST['fb_change']);
		$seo_descr=mysql_real_escape_string($_POST['seo_descr']);
		$seo_keywords=mysql_real_escape_string($_POST['seo_keywords']);
		$tagname_1=mysql_real_escape_string($_POST['tagname_1']);
		$tagname_2=mysql_real_escape_string($_POST['tagname_2']);
		$tagname_3=mysql_real_escape_string($_POST['tagname_3']);
		$tagname_4=mysql_real_escape_string($_POST['tagname_4']);
		$tagname_5=mysql_real_escape_string($_POST['tagname_5']);
		$tagname_6=mysql_real_escape_string($_POST['tagname_6']);


		$id_category=mysql_real_escape_string($_POST['id_category']);
		$type=mysql_real_escape_string($_POST['type']);
		
		if(empty($page_name)){$messege = 'Необходимо ввести назвние.';}
		else{

                        
			mysql_query("INSERT INTO ".$db_pref."articles(id, status,  name, content, seo_title, seo_description, seo_keywords, id_category, date_added, author, type, fb_change, tag_name_1, tag_name_2, tag_name_3, tag_name_4, tag_name_5, tag_name_6) VALUES ('0', '".$_POST['show_page']."',  '".$page_name."', '".$page_content."', '".$seo_title."', '".$seo_descr."', '".$seo_keywords."' , '".$id_category."', '".date('Y-m-d H:i:s')."', '".$id_user."', '".$type."',  '".$fb_change."', '".$tagname_1."', '".$tagname_2."', '".$tagname_3."', '".$tagname_4."', '".$tagname_5."', '".$tagname_6."' )") or die (mysql_error());
			$last_id=mysql_insert_id();


			
			if($_FILES['file']['size']>0) {
				$file_type = str_replace("image/", "", $_FILES['file']['type']);
				$new_file_name='article-'.$last_id.".".$file_type;
				move_uploaded_file($_FILES["file"]["tmp_name"],"../uploads/".$new_file_name);	

				require_once 'PHPThumb-master/src/ThumbLib.inc.php';
				$f="../uploads/86_".$new_file_name;
				try{
					$thumb = PhpThumbFactory::create("../uploads/".$new_file_name);
				}catch (Exception $e){
					// handle error here however you'd like
				}
				//$thumb->resize(24, 18);
				$thumb->adaptiveResize(86, 86);
				$thumb->save($f);

				mysql_query("Update ".$db_pref."articles set image='$new_file_name' where id='$last_id'") or die("er2");
			}
			
			if($_FILES['file3']['size']>0) {
				$file_type = str_replace("image/", "", $_FILES['file3']['type']);
				$new_file_name='article_big-'.$last_id.".".$file_type;
				move_uploaded_file($_FILES["file3"]["tmp_name"],"../uploads/".$new_file_name);	

				require_once 'PHPThumb-master/src/ThumbLib.inc.php';
				$f="../uploads/200_".$new_file_name;
				try{
					$thumb = PhpThumbFactory::create("../uploads/".$new_file_name);
				}catch (Exception $e){
					// handle error here however you'd like
				}
				//$thumb->resize(24, 18);
				$thumb->adaptiveResize(200, 200);
				$thumb->save($f);

				mysql_query("Update ".$db_pref."articles set image_big='$new_file_name' where id='$last_id'") or die("er2");
			}
			
			if($_FILES['file2']['size']>0) {
				//$file_type = str_replace("image/", "", $_FILES['file2']['type']);
				//$new_file_name='article-'.$last_id.".".$file_type;
				move_uploaded_file($_FILES["file2"]["tmp_name"],"../uploads/".$last_id.'-'.$_FILES["file2"]["name"]);	

				
				$audio=$last_id.'-'.$_FILES["file2"]["name"];
				mysql_query("Update ".$db_pref."articles set audio='$audio' where id='$last_id'") or die("er2");
			}
			
			 $messege="Информация сохранена";
			 
			 header("Location: article_edit.php?id=".$last_id); exit;
		}
	}


	
	include('header.php');
?>
	<?php if(isset($messege)){echo'<div class="alert alert-info">'.$messege.'</div>';}?>
	<form method='post' action='article_add.php'   enctype='multipart/form-data'>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="js/typeahead.min.js"></script>
    <script>
    $(document).ready(function(){
    $('input.typeahead').typeahead({
        name: 'prof1',
        remote:'sear.php?key=%QUERY',
        limit : 10
    });
});
    </script>
  <style type="text/css">
.bs-example{
	font-family: sans-serif;
	position: relative;
	margin: 50px;
}
.typeahead, .tt-query, .tt-hint {
	border: 2px solid #CCCCCC;
	border-radius: 8px;
	font-size: 24px;
	height: 30px;
	line-height: 30px;
	outline: medium none;
	padding: 8px 12px;
	width: 396px;
}
.typeahead {
	background-color: #FFFFFF;
}
.typeahead:focus {
	border: 2px solid #0097CF;
}
.tt-query {
	box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
}
.tt-hint {
	color: #999999;
}
.tt-dropdown-menu {
	background-color: #FFFFFF;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-radius: 8px;
	box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	margin-top: 12px;
	padding: 8px 0;
	width: 422px;
}
.tt-suggestion {
	font-size: 24px;
	line-height: 24px;
	padding: 3px 20px;
}
.tt-suggestion.tt-is-under-cursor {
	background-color: #0097CF;
	color: #FFFFFF;
}
.tt-suggestion p {
	margin: 0;
}
</style>


	        <script type="text/javascript" src="js/translite.js"></script>
		<table class="table" >

			<tr>
				<td>Название</td>
				
                                <td><input type="text" name="page_name" id="name" onkeyup="translit()" class="form-control" value="<?php echo (isset($page_name))?$page_name:''; ?>" /></td>
			</tr>
			<tr>
				<td>Контент</td>
				<td><textarea class="editme form-control" name="page_content"><?php echo (isset($page_content))?$page_content:''; ?></textarea></td>
			</tr>

			<tr>
				<td>Аудио (вставить в текст макросом {audio})</td>
				<td><input type="file" name="file2" class="form-control"/></td>
			</tr>	
			<tr>
				<td class="tlr">Категория</td>
				<td><select name="id_category" class="form-control">
					<?php
						$query_limited = "select * from ".$db_pref."categories where type=0 ORDER BY name";
						$final_result = mysql_query($query_limited);
						while ($row = mysql_fetch_assoc($final_result)) {
							if($row['id']!=5 && $row['id']!=6 && $row['id']!=12){
								echo"<option value='".$row['id']."'>".$row['name']."</option>";	
							}
						}
					?>
				</select></td> 
			</tr>
			<tr>
				<td>Миниатюра</td>
				<td><input type="file" name="file" class="form-control"/></td>
			</tr>
			<tr>
				<td>Картинка сео</td>
				<td><input type="file" name="file3" class="form-control"/></td>
			</tr>	
			<tr>
				<td class="tlr">Тип</td>
				<td><select name="type" class="form-control">
					<option value="0">текст</option>
					<option value="1">видео</option>
					<option value="2">аудио</option>
				</select></td> 
			</tr>		
			<tr>
				<td class="tlr">Статус</td>
				<td><select name="show_page" class="form-control">
					<option value="1">показать</option>
					<option value="0">скрыть</option>
				</select></td> 
			</tr>
			<tr>
				<td class="tlr">SEO Title:</td>
				<td><input type="text" name="seo_title" id="alias" class="form-control lang_sw" value="<?php echo (isset($seo_title))?$seo_title:''; ?>" /></td>
			</tr>
			<tr>
				<td class="tlr">SEO Description:</td>
				<td><textarea class="form-control" name="seo_descr"><?php echo (isset($seo_descr))?$seo_descr:''; ?></textarea></td>
			</tr>
			<tr>
				<td class="tlr">SEO Keywords:</td>
				<td><textarea class="form-control" name="seo_keywords"><?php echo (isset($seo_keywords))?$seo_keywords:''; ?></textarea></td></td>
			</tr>
                        <tr>
                                <td class="tlr">SEO Tags:</td>
				<td>
				    <input type="text" name="tagname_1" class="typeahead tt-query" value="<?php echo (isset($tag_name))?$tag_name:''; ?>" autocomplete="off" spellcheck="false" placeholder="Введите тег">
				    <input type="text" name="tagname_2" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег">
                                    <input type="text" name="tagname_3" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег">
                                    <input type="text" name="tagname_4" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег">
                                    <input type="text" name="tagname_5" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег">
                                    <input type="text" name="tagname_6" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег"></td>
                        </tr>
                        <input type='hidden' name="fb_change" class="form-control" value='1' />
			<tr>
				<td></td>
				<td>
					<input type='hidden' name='action' value='add' />
					<input type='submit' value='Сохранить' class='btn btn-primary pull-right' />
					<input type='button' value='Отмена' class='btn btn-default pull-right' onclick="location.href='article_list.php'" style="margin-right:20px;" />
				</td>
			</tr>

		</table>
	</form>

</div>

<?php include('footer.php'); ?>