<?php
	include('config.php');

	checkPermission(1);
	$active_page='cms';
		
	$title_page='Управление страницами / Добавление страницы';

	if(isset($_POST['action']) and $_POST['action']=="add"){
		
		$page_name=mysql_real_escape_string($_POST['page_name']);			
		$page_content=mysql_real_escape_string($_POST['page_content']);
		$seo_title=mysql_real_escape_string($_POST['seo_title']);
		$seo_descr=mysql_real_escape_string($_POST['seo_descr']);
		$seo_keywords=mysql_real_escape_string($_POST['seo_keywords']);
		
		mysql_query("INSERT INTO ".$db_pref."pages(id, show_page, link, name, content, seo_title, seo_description, seo_keywords) VALUES ('0', '".$_POST['show_page']."', '".urlencode($_POST['page_link'])."', '".$page_name."', '".$page_content."', '".$seo_title."', '".$seo_descr."', '".$seo_keywords."' )") or die (mysql_error());
		$last_id=mysql_insert_id();
		
		 $messege="Информация сохранена";
	}


	
	include('header.php');
?>
 <?php if(isset($messege)){echo'<div class="alert alert-info">'.$messege.'</div>';}?>

		<form method='post' action='page_add.php'>
		<script type="text/javascript" src="js/translite.js"></script>
			<table class="table" >

				<tr>
					<td>Название</td>
					<td><input type="text" name="page_name" id="name" onkeyup="translit()" class="form-control"/></td>
				</tr>
				<tr>
					<td>Контент</td>
					<td><textarea class="editme form-control" name="page_content"></textarea></td>
				</tr>
				<tr>
					<td class="tlr">Ссылка</td>
					<td><input type="text" name="page_link"  class="form-control" /></td>                    
				</tr>
				<tr>
					<td class="tlr">Видимость</td>
					<td><select name="show_page" class="form-control">
						<option value="1">показать</option>
						<option value="0">скрыть</option>
					</select></td> 
				</tr>
				<tr>
					<td class="tlr">SEO Title:</td>
					<td><input type="text" name="seo_title" id="alias" class="form-control lang_sw" /></td>
				</tr>
				<tr>
					<td class="tlr">SEO Description:</td>
					<td><textarea class="form-control" name="seo_descr"></textarea></td>
				</tr>
				<tr>
					<td class="tlr">SEO Keywords:</td>
					<td><textarea class="form-control" name="seo_keywords"></textarea></td></td>
				</tr>
				
				
				<tr>
                	<td></td>
					<td>
						
						<input type='hidden' name='action' value='add' />
						<input type='submit' value='Сохранить' class='btn btn-primary pull-right' />
						 <input type='button' value='Отмена' class='btn btn-default pull-right' onclick="location.href='page_list.php'" style="margin-right:20px;" />
						
					</td>
				</tr>

			</table>
            </form>

</div>

<?php include('footer.php'); ?>