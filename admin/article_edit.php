<?php
	include('config.php');
	checkPermission(1);

	if(isset($_GET['id'])){
		$id=(int)$_GET['id'];
		
	} elseif(isset($_POST['id'])){
		$id=(int)$_POST['id'];
		
	} else{
		header("Location: article_list.php"); exit;
	}
	
	
	$active_page='materials';
		
	$title_page='Материалы / Редактирование статьи';

	if(isset($_POST['action'])){
		if($_POST['action']=="edit"){
			
			$last_id=$id;
			$_POST['show_page'] = (int) $_POST['show_page'];
			$page_name=mysql_real_escape_string($_POST['page_name']);			
			$page_content=mysql_real_escape_string($_POST['page_content']);
			$seo_title=mysql_real_escape_string($_POST['seo_title']);
			$fb_change=mysql_real_escape_string($_POST['fb_change']);
			$seo_descr=mysql_real_escape_string($_POST['seo_descr']);
			$seo_keywords=mysql_real_escape_string($_POST['seo_keywords']);
			$tag_1=mysql_real_escape_string($_POST['tagname_1']);
			$tag_2=mysql_real_escape_string($_POST['tagname_2']);
			$tag_3=mysql_real_escape_string($_POST['tagname_3']);
			$tag_4=mysql_real_escape_string($_POST['tagname_4']);
			$tag_5=mysql_real_escape_string($_POST['tagname_5']);
			$tag_6=mysql_real_escape_string($_POST['tagname_6']);

			$artname=mysql_real_escape_string($_POST['name']);
			
			$id_category=mysql_real_escape_string($_POST['id_category']);
			$type=mysql_real_escape_string($_POST['type']);

			if(empty($page_name)){$messege = 'Необходимо ввести назвние.';}
			else{
			
			mysql_query("update ".$db_pref."articles set 
					status='".$_POST['show_page']."',
            		name='".$artname."', 
					content='".$page_content."', 
					seo_title='".$seo_title."',
					fb_change='".$fb_change."', 
					seo_description='".$seo_descr."', 
					seo_keywords='".$seo_keywords."',
					tag_name_1='".$tag_1."',
					tag_name_2='".$tag_2."',
					tag_name_3='".$tag_3."',
					tag_name_4='".$tag_4."',
					tag_name_5='".$tag_5."',
					tag_name_6='".$tag_6."', 
					id_category='".$id_category."', 
					type='".$type."'
					where id=".$id) or die(mysql_error());
					
			if($_FILES['file']['size']>0) {
				$file_type = str_replace("image/", "", $_FILES['file']['type']);
				$bignum = hexdec( substr(md5(microtime()), 0, 15) );
				//$id_user = (int)$bignum;
				$new_file_name='article-'.$last_id.".".$file_type;
				move_uploaded_file($_FILES["file"]["tmp_name"],"../uploads/".$new_file_name);	

				require_once 'PHPThumb-master/src/ThumbLib.inc.php';
				$f="../uploads/86_".$new_file_name;
				try{
					$thumb = PhpThumbFactory::create("../uploads/".$new_file_name);
				}catch (Exception $e){
					// handle error here however you'd like
				}
				//$thumb->resize(24, 18);
				$thumb->adaptiveResize(86, 86);
				$thumb->save($f);

				mysql_query("Update ".$db_pref."articles set image='$new_file_name' where id='$last_id'") or die("er2");
			}

			if($_FILES['file3']['size']>0) {
				$file_type = str_replace("image/", "", $_FILES['file3']['type']);
				$new_file_name='article_big-'.$last_id.".".$file_type;
				move_uploaded_file($_FILES["file3"]["tmp_name"],"../uploads/".$new_file_name);
				mysql_query("Update ".$db_pref."articles set image_big='$new_file_name' where id='$last_id'") or die("er2");	

				require_once 'PHPThumb-master/src/ThumbLib.inc.php';
				$f="../uploads/200_".$new_file_name;
				try{
					$thumb = PhpThumbFactory::create("../uploads/".$new_file_name);
				}catch (Exception $e){
					// handle error here however you'd like
				}
				//$thumb->resize(24, 18);
				$thumb->adaptiveResize(200, 200);
				$thumb->save($f);

				mysql_query("Update ".$db_pref."articles set image_big='$new_file_name' where id='$last_id'") or die("er2");
			}
		
			if($_FILES['file2']['size']>0) {
				//$file_type = str_replace("image/", "", $_FILES['file2']['type']);
				//$new_file_name='article-'.$last_id.".".$file_type;
				move_uploaded_file($_FILES["file2"]["tmp_name"],"../uploads/".$last_id.'-'.$_FILES["file2"]["name"]);	

				
				$audio=$last_id.'-'.$_FILES["file2"]["name"];
				mysql_query("Update ".$db_pref."articles set audio='$audio' where id='$last_id'") or die("er2");
			}
		

			$messege="Информация сохранена";
			}
		}elseif($_POST['action']=="delete"){
			
			$sql = "DELETE FROM ".$db_pref."articles WHERE id=".$id;
			$result = @mysql_query($sql,$connection) or die("er");
			header("Location: article_list.php"); exit;
		}
	}
	
	if($_GET['act']=="delete"){
		
			
			$sql = "DELETE FROM ".$db_pref."articles WHERE id=".$id;
			$result = @mysql_query($sql,$connection) or die("er");
			header("Location: article_list.php"); exit;
		}


	if(is_numeric($id)){
		$sql_1= "SELECT * FROM ".$db_pref."articles WHERE id=".$id;
		$result_1 = @mysql_query($sql_1);
		$row_1 = mysql_fetch_array($result_1);		
		
		if(!isset($row_1['id'])){
			header("Location: article_list.php");
			exit;
		}else{
			
			$show_page=$row_1['status'];			
			$p_name=stripslashes($row_1['name']);
			$p_content=stripslashes($row_1['content']);
			$p_seo_title=stripslashes($row_1['seo_title']);
			$p_seo_descr=stripslashes($row_1['seo_description']);
			$p_seo_keywords=stripslashes($row_1['seo_keywords']);
			$p_tagname_1=stripslashes($row_1['tag_name_1']);
			$p_tagname_2=stripslashes($row_1['tag_name_2']);
			$p_tagname_3=stripslashes($row_1['tag_name_3']);
			$p_tagname_4=stripslashes($row_1['tag_name_4']);
			$p_tagname_5=stripslashes($row_1['tag_name_5']);
			$p_tagname_6=stripslashes($row_1['tag_name_6']);

			$p_artname=stripslashes($row_1['name']);
			$fb_change=stripslashes($row_1['fb_change']);
			$id_category=$row_1['id_category'];
			$type=$row_1['type'];
			$audio=$row_1['audio'];
			$image=$row_1['image'];
			$image_big=$row_1['image_big'];
			if($image!=''){
				$img='<img src="../uploads/86_'.$image.'" />';
				$img_big='<img src="../uploads/200_'.$image_big.'" />';
				}
				else $img='';
		
		}
		
	
	}else{
		header("Location: article_list.php");
		exit;
	}

	include('header.php');
	
	
?>


     <?php if(isset($messege)){echo'<div class="alert alert-info">'.$messege.'</div>';}?>

		<form method='post' action='article_edit.php?id=<?php echo $id?>'   enctype='multipart/form-data'>

		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="js/typeahead.min.js"></script>
    <script>
    $(document).ready(function(){
    $('input.typeahead').typeahead({
        name: 'prof1',
        remote:'sear.php?key=%QUERY',
        limit : 10
    });
});
    </script>
  <style type="text/css">
.bs-example{
	font-family: sans-serif;
	position: relative;
	margin: 50px;
}
.typeahead, .tt-query, .tt-hint {
	border: 2px solid #CCCCCC;
	border-radius: 8px;
	font-size: 24px;
	height: 30px;
	line-height: 30px;
	outline: medium none;
	padding: 8px 12px;
	width: 396px;
}
.typeahead {
	background-color: #FFFFFF;
}
.typeahead:focus {
	border: 2px solid #0097CF;
}
.tt-query {
	box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
}
.tt-hint {
	color: #999999;
}
.tt-dropdown-menu {
	background-color: #FFFFFF;
	border: 1px solid rgba(0, 0, 0, 0.2);
	border-radius: 8px;
	box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	margin-top: 12px;
	padding: 8px 0;
	width: 422px;
}
.tt-suggestion {
	font-size: 24px;
	line-height: 24px;
	padding: 3px 20px;
}
.tt-suggestion.tt-is-under-cursor {
	background-color: #0097CF;
	color: #FFFFFF;
}
.tt-suggestion p {
	margin: 0;
}
</style>
			<table class="table" >

				<tr>
					<td>Название</td>
                                        
	                                <script type="text/javascript" src="js/translite.js"></script>
					<td>
                    	<?php 
							echo '<input type="text" name="name" id="name" onkeyup="translit()" class="form-control"  value="'.$p_artname.'"/>';
                                                        echo '<input type="hidden" name="page_name" id="alia" class="form-control"  value="'.$p_name.'"/>';
						?>                    		
                    </td>                    
				</tr>
				<tr>
					<td class="tlr">Контент</td>
					<td>
                    	<?php 
							echo '<textarea class="editme form-control" name="page_content">'.$p_content.'</textarea></div>';
							
						?>
                     </td>
				</tr>
                                <tr>
					<td>Аудио (вставить в текст макросом {audio})
                    <?php 
						if($audio) echo '<a href="../uploads/'.$audio.'" target="_blank">'.$audio.'</a>';
					?>
                    </td>
					<td><input type="file" name="file2" class="form-control"/></td>
				</tr>
                 <tr>
					<td class="tlr">Категория</td>
					<td><select name="id_category" class="form-control">
						<?php
							$query_limited = "select * from ".$db_pref."categories where type=0 ORDER BY name";
							$final_result = mysql_query($query_limited);
							while ($row = mysql_fetch_assoc($final_result)) {
								if($row['id']==$id_category) $s=' selected'; else $s='';
								if($row['id']!=5 && $row['id']!=6 && $row['id']!=12){
									echo"<option value='".$row['id']."' ".$s.">".$row['name']."</option>";	
								}
							}
						?>
					</select></td> 
				</tr>	
                
                <tr>
					<td>Миниатюра <?php echo $img?></td>
					<td><input type="file" name="file" class="form-control"/></td>
				</tr>
				<tr>
					<td>Картинка для соцсетей <?php echo $img_b?></td>
					<td><input type="file" name="file3" class="form-control"/></td>
				</tr>	
                <tr>
					<td class="tlr">Тип</td>
					<td><select name="type" class="form-control">
						<option value="0"<?php if($type==0){echo" selected";}?>>текст</option>
						<option value="1"<?php if($type==1){echo" selected";}?>>видео</option>
                        <option value="2"<?php if($type==2){echo" selected";}?>>аудио</option>
					</select></td> 
				</tr>		
				
				<tr>
					<td class="tlr">Статус</td>
					<td><select name="show_page" class="form-control">
						<option value="1"<?php if($show_page==1){echo" selected";}?>>показать</option>
						<option value="0"<?php if($show_page==0){echo" selected";}?><?php if($system==1) {echo 'disabled';}?>>скрыть</option>
					</select></td>
                    
				</tr>
				<tr>
					<td class="tlr">SEO Title:</td>
					<td>
                        <?php 
							echo '<input type="text" name="seo_title" id="alias" class="form-control"  value="'.$p_seo_title.'"/>';
							
						?>
                    </td>                   
				</tr>
				<tr>
					<td class="tlr">SEO Description:</td>
					<td>
                        <?php 
							echo '<input type="text" name="seo_descr" class="form-control"  value="'.$p_seo_descr.'"/>';
							
						?>
                    </td>
				</tr>
				<tr>
					<td class="tlr">SEO Keywords:</td>
					<td>
                        <?php 
							echo '<input type="text" name="seo_keywords" class="form-control"  value="'.$p_seo_keywords.'"/>';
							
						?>
                    </td>
				</tr>

				<tr>
                                <td class="tlr">SEO Tags:</td>
				<td>
					<?php 
							echo '<input type="text" name="tagname_1" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег" value="'.$p_tagname_1.'"/>
								<input type="text" name="tagname_2" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег" value="'.$p_tagname_2.'"/>
								<input type="text" name="tagname_3" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег" value="'.$p_tagname_3.'"/>
								<input type="text" name="tagname_4" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег" value="'.$p_tagname_4.'"/>
								<input type="text" name="tagname_5" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег" value="'.$p_tagname_5.'"/>
								<input type="text" name="tagname_6" class="typeahead tt-query" autocomplete="off" spellcheck="false" placeholder="Введите тег" value="'.$p_tagname_6.'"/>';
							
						?>
				    </td>
                        </tr>
				
				<input type='hidden' name="fb_change" class="form-control" value='1' />
				<tr>
                	<td></td>
					<td>
						<input type='hidden' name='page_id' value='<?php echo $id; ?>' /> 
						<input type='hidden' name='action' value='edit' />
                        <a href="article_edit.php?id=<?php echo $id?>&act=delete" class="btn btn-danger pull-left"  onclick='javascript:return confirm("Вы уверены?")'>Удалить</a>
                        <input type='submit' value='Сохранить' class='btn btn-primary pull-right' />
						 <input type='button' value='Отмена' class='btn btn-default pull-right' onclick="location.href='article_list.php'" style="margin-right:20px;" />
						 

					</td>
				</tr>

			</table>
            </form>



<?php include('footer.php'); ?>
