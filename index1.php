<!DOCTYPE html dir="rtl" lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if it IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if it IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <style>
	.modalDialog {
		position: fixed;
		font-family: Arial, Helvetica, sans-serif;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		background: rgba(0,0,0,0.8);
		z-index: 99999;
		-webkit-transition: opacity 400ms ease-in;
		-moz-transition: opacity 400ms ease-in;
		transition: opacity 400ms ease-in;
		display: none;
		pointer-events: none;
	}

	.modalDialog:target {
		display: block;
		pointer-events: auto;
	}

	.modalDialog > div {
		width: 300px;
		position: relative;
		margin: 10% auto;
		padding: 5px 20px 13px 20px;
		border-radius: 10px;
		background: #fff;
		background: -moz-linear-gradient(#fff, #999);
		background: -webkit-linear-gradient(#fff, #999);
		background: -o-linear-gradient(#fff, #999);
	}
	
	
	.modal_req_Dialog {
		position: fixed;
		font-family: Arial, Helvetica, sans-serif;
		top: 0;
		right: 0;
		bottom: 0;
		left: 0;
		background: rgba(0,0,0,0.8);
		z-index: 99999;
		-webkit-transition: opacity 400ms ease-in;
		-moz-transition: opacity 400ms ease-in;
		transition: opacity 400ms ease-in;
		display: none;
		pointer-events: none;
	}

	.modal_req_Dialog:target {
		display: block;
		pointer-events: auto;
	}

	.modal_req_Dialog > div {
		width: 85%;
		position: relative;
		margin: 10% auto;
		/*padding: 5px 20px 13px 20px;*/
		border-radius: 4px;
		background: #fff;
		background: -moz-linear-gradient(#fff, #999);
		background: -webkit-linear-gradient(#fff, #999);
		background: -o-linear-gradient(#fff, #999);
	}

    @media only screen and (max-width: 768px) {
      /* For mobile phones: */
     .modal_req_Dialog > div {
        width: 200%;
      }
     .white{
        width: 500px;
      }

    }

	.close {
		background: #606061;
		color: #FFFFFF;
		line-height: 25px;
		position: absolute;
		right: -12px;
		text-align: center;
		top: -10px;
		width: 24px;
		text-decoration: none;
		font-weight: bold;
		-webkit-border-radius: 12px;
		-moz-border-radius: 12px;
		border-radius: 12px;
		-moz-box-shadow: 1px 1px 3px #000;
		-webkit-box-shadow: 1px 1px 3px #000;
		box-shadow: 1px 1px 3px #000;
	}

	.close:hover { background: #00d9ff; }
	</style>
        <meta charset="utf-8">
        <title>Flex - Responsive HTML Template</title>
    	<meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- 
Flex Template 
https://templatemo.com/tm-406-flex
-->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/templatemo_misc.css">
        <link rel="stylesheet" href="css/templatemo_style.css">

        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an outdated browser. <a href="http://browsehappy.com/">Upgrade your browser today</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to better experience this site.</p>
        <![endif]-->


        <div class="site-main" id="sTop">
            <div class="site-header">
                <!--<div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <ul class="social-icons">
                                <li><a href="#" class="fa fa-facebook"></a></li>
                                <li><a href="#" class="fa fa-twitter"></a></li>
                                <li><a href="#" class="fa fa-dribbble"></a></li>
                                <li><a href="#" class="fa fa-linkedin"></a></li>
                            </ul>
                        </div> 
                    </div> 
                </div>--> 
                
                <div class="main-header">
                    <div class="container">
                        <div id="menu-wrapper">
                            <div class="row">
                                <div class="logo-wrapper col-md-2 col-sm-2 text-right">
                                    <h1>
                                        <a href="#"></a>
                                    </h1>
                                </div> <!-- /.logo-wrapper -->
                                <div class="contact-wrapper text-right">
                                    
                                        
                                        <a style="background: unset;">055-553-75-79</a>
                                        
                                        <div style="padding-right: 22px">
                                        <a style="text-decoration: none;" href="https://api.whatsapp.com/send?phone=972555537579&text=שלום!&source=&data="><span style="border-bottom: 1px dashed;border-bottom-width: 1px; border-bottom-style: dashed; border-bottom-color: initial; font-size: 8px;">whatsapp</span></a>
                                        </div>
                                        
                                        
                                            <!--<a style="text-decoration: none;" href="https://api.whatsapp.com/send?phone=972555537579&text=שלום!&source=&data=">-->
                                            <!--    <span style="border-bottom: 1px dashed;border-bottom-width: 1px; border-bottom-style: dashed; border-bottom-color: initial; font-size: 8px;">whatsapp</span>-->
                                            <!--</a>-->
                                        
                                </div>
                                <div style="text-align: center;"><input type="button" onclick="location.href='/#requestModal'" class="mainBtn" id="form" value="שלח בקשה">
                                &nbsp;<a href="/price_gezerpc.xlsx">מחירון להורדה </a>
                                </div>
                                <!-- <div class="col-md-10 col-sm-10 main-menu text-right">
                                    <div class="toggle-menu visible-sm visible-xs"><i class="fa fa-bars"></i></div>
                                    <ul class="menu-first">
                                        <li class="active"><a href="#">Home</a></li>
                                        <li><a href="#services">Services</a></li>
                                        <li><a href="#portfolio">Portfolio</a></li>
                                        <li><a href="#our-team">Team</a></li>
                                        <li><a href="https://fb.com/templatemo" class="external" rel="nofollow">External</a></li> 
                                        <li><a href="#contact">Contact</a></li>                                 
                                    </ul>                                    
                                </div> --> <!-- /.main-menu -->
                            </div> <!-- /.row -->
                        </div> <!-- /#menu-wrapper -->                        
                    </div> <!-- /.container -->
                </div> <!-- /.main-header -->
            </div> <!-- /.site-header -->
            <div class="site-slider">
                <div class="slider">
                    <div class="flexslider">
                        <ul class="slides">
                            <li>
                                <div class="overlay"></div>
                                <img src="images/slide1.jpg" alt="">
                                <!--<div class="slider-caption visible-md visible-lg">
                                    <h2>Digital Marketing</h2>
                                    <p>more visitors to your website</p>
                                    <a href="#" class="slider-btn">Let us design!</a>
                                </div>-->
                            </li>
                            <li>
                                <div class="overlay"></div>
                                <img src="images/slide2.jpg" alt="">
                                <!--<div class="slider-caption visible-md visible-lg">
                                    <h2>Responsive HTML CSS</h2>
                                    <p>Download and use it for your site</p>
                                    <a href="#" class="slider-btn">Go to Portfolio</a>
                                </div>-->
                            </li>
                            <li>
                                <div class="overlay"></div>
                                <img src="images/slide3.jpg" alt="">
                                <!--<div class="slider-caption visible-md visible-lg">
                                    <h2>Pro Level Design</h2>
                                    <p>High standard work</p>
                                    <a href="#" class="slider-btn">Mobile Website</a>
                                </div>
                            </li>
                        </ul>
                    </div> <!-- /.flexslider -->
                </div> <!-- /.slider -->
            </div> <!-- /.site-slider -->
        </div> <!-- /.site-main -->


        <div class="content-section" id="services">
            <div class="container">
                <div class="row">
                    <div class="experience col-md-12 text-center">
                       <h2>ניסיון של <span class="years">11</span> שנים</h2>
                       
                    </div> <!-- /.heading-section -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="heading-section col-md-12 text-center">
                        <h2>השירות שלנו</h2>
                        
                    </div> <!-- /.heading-section -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-1">
                            <div class="service-icon">
                                <i class="fa fa-desktop fa-3x"></i>
                            </div> <!-- /.service-icon -->
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>מחשב</h3>
                                   <p>שקע טעינה</p> 
                                        <p>החלפת מסך מגע</p>
                                        <p>תיקון לוח ומים</p>
                                        <p>תיקון דיסק קשיח</p>
                                        <p>התקנות אופיס ואנטי וירוס</p>
                                     
                                </div>
                            </div> <!-- /.service-content -->
                        </div> <!-- /#service-1 -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-2">
                            <div class="service-icon">
                                <i class="fa fa-tablet fa-3x"></i>
                            </div> <!-- /.service-icon -->
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>טאבלט</h3>
                                   <p>שקע טעינה 
                                        <br>החלפת מסך</br>
                                        תיקון לוח ומים</br>
                                        פריצה סיסמה</br>
                                    </p>  
                                </div>
                            </div> <!-- /.service-content -->
                        </div> <!-- /#service-1 -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-3">
                            <div class="service-icon">
                                <i class="fa fa-mobile-phone fa-3x"></i>
                            </div> <!-- /.service-icon -->
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>טלפון</h3>
                                   <p>שקע טעינה 
                                        <br>תיקון שמע</br>
                                        תיקון שקע אזניות</br>
                                        תיקון מיקרופון</br>
                                        סידור כפתורים</br>
                                    </p>
                                </div>
                            </div> <!-- /.service-content -->
                        </div> <!-- /#service-1 -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-4">
                            <div class="service-icon">
                                <i class="fa fa-camera fa-3x"></i>
                            </div> <!-- /.service-icon -->
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>מצלמה</h3>
                                   <p>שקע טעינה 
                                        <br>החלפת מסך
                                        <br>תיקון מים וקורוזיה
                                        <br>סידור כפתורים
                                        <br>תיקון עדשה והחלפת עדשה
                                    </p>
                                </div>
                            </div> <!-- /.service-content -->
                        </div> <!-- /#service-1 -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-5">
                            <div class="service-icon">
                                <p></p>
                                <i class="fa fa-music fa-3x"></i>
                            </div> <!-- /.service-icon -->
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>נגן</h3>
                                   <p>שקע טעינה 
                                        <br>שקע אוזניות
                                        <br>החלפת מסך
                                        <br>תיקון לוח ומים
                                        <br>לסדר כפתור
                                    </p> 
                                </div>
                            </div> <!-- /.service-content -->
                        </div> <!-- /#service-1 -->
                    </div> <!-- /.col-md-3 -->
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item" id="service-6">
                            <div class="service-icon">
                                <i class="fa fa-gears fa-3x"></i>
                            </div> <!-- /.service-icon -->
<span ontouchmove="">
                            <div class="service-content">
                                <div class="inner-service">
                                   <h3>עבודה שונה</h3>
                                   <p>מתקנים רמקולים
                                        <br>תיקון גיים בוי
                                        <br>שיחזור מידע
                                        <br>הלחמות אחרות
                                    </p> 
                                </div>
                            </div> <!-- /.service-content --></span>
                        </div> <!-- /#service-6 -->
                    </div> <!-- /.col-md-3 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /#services -->

        
                

        <div class="content-section" id="contact">
            <div class="container">
                <div class="row">
                    <!--<div class="heading-section col-md-12 text-center">
                        <h2>Contact</h2>
                        <p>Send a message to us</p>
                    </div>--> <!-- /.heading-section -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                       <div class="googlemap-wrapper">
                            
                            <div id="wrapper-9cd199b9cc5410cd3b1ad21cab2e54d3">
        <div class="map-canvas" style="border: 10px solid #efefef; position: relative; overflow: hidden;" id="map-9cd199b9cc5410cd3b1ad21cab2e54d3"></div><script>(function () {
        var setting = {"height":380,"width":2048,"zoom":18,"queryString":"בית ישראל 2, ירושלים, יאשראך","place_id":"ChIJ1YpshdwpAxURWlVT32aA0cA","satellite":false,"centerCoord":[31.78808215984163,35.22365279999999],"cid":"0xc0d18066df53555a","lang":"he","cityUrl":"/israel/jerusalem","cityAnchorText":"Карта [CITY1], Israel, יאשראל","id":"map-9cd199b9cc5410cd3b1ad21cab2e54d3","embed_id":"212631"};
        var d = document;
        var s = d.createElement('script');
        s.src = 'https://1map.com/js/script-for-user.js?embed_id=212631';
        s.async = true;
        s.onload = function (e) {
          window.OneMap.initMap(setting)
        };
        var to = d.getElementsByTagName('script')[0];
        to.parentNode.insertBefore(s, to);
      })();</script></div>
                        </div> <!-- /.googlemap-wrapper -->
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
<p dir="rtl" class="text-right">
    
    <span style="font-size: 26px; font-weight: bold; color: #ec523f;">גזר מחשבים</span>
    <span style="font-size: 20px;">
מספקת שירותי תיקון איכותיים בעיר ירושלים. הרצון של שירות לקוחות איכותי השראה לנו להתחיל את העסק הזה ומסייע לנו להתקדם כל יום. לקוחות מעריכים את המהירות של השירותים שלנו, והכי חשוב - הם מרוצים תוצאה של העבודה שלנו. בקר בסדנה או התקשר אלינו.
</span>
</p>
                   
                        <!--<ul class="contact-info">
                            <li dir="rtl">טלפןן: <a href="https://api.whatsapp.com/send?phone=972555537579&text=%D7%A9%D7%9C%D7%95%D7%9D!&source=&data=">055-553-75-79</a></li>
                            <li dir="rtl"><a href="mailto:Computers1israel@gmail.com">Computers1israel@gmail.com</a></li>
                            <li dir="rtl">כתובת: בית ישראל 2, ירושלים</li>
                        </ul>-->
                        <!-- spacing for mobile viewing --><br><br>
                    </div> <!-- /.col-md-7 -->
                    <div class="col-md-5 col-sm-6">
                        <!--<div class="contact-form">
                            <form method="post" name="contactform" id="contactform">
                                <p dir="rtl">
                                    <input name="name" type="text" required id="name" placeholder="שם">
                                </p>
                                <p dir="rtl">
                                    <input name="email" type="text" required id="email" placeholder="דואר אלקטרוני"> 
                                </p>
                                <p dir="rtl">
                                    <input name="subject" type="text" required id="subject" placeholder="נושא"> 
                                </p>
                                <p dir="rtl">
                                    <textarea name="comments" required id="comments" placeholder="הוֹדָעָה"></textarea>    
                                </p>
                                <input type="submit" class="mainBtn" id="submit" value="לשלוח הודעה">
                            </form>
                        </div>--> <!-- /.contact-form -->
                    </div> <!-- /.col-md-5 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /#contact -->
        
        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-xs-12 text-left">
                        <span>Copyright &copy; 2020 Gezer Computers</span>
                  </div> <!-- /.text-center -->
                    <div class="col-md-4 hidden-xs text-right">
                        <a href="#top" id="go-top">Back to top</a>
                    </div> <!-- /.text-center -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /#footer -->
        
        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="js/bootstrap.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        
        

        <div id="sentModal" class="modalDialog">
        	<div>
        		<a href="#close" title="Закрыть" class="close" style="
    opacity: 1.2;
">X</a>
        		<div class="service-content">
        		<h2>הודעה</h2>
        		<p>!הודעה נשלכה בהצלחה</p>
        		</div>
        	</div>
        	
        </div>
        
        <div id="advModal" class="modalDialog">
        	<div>
        		<a href="#close" title="Закрыть" class="close" style="
    opacity: 1.2;
">X</a>
        		<div class="service-content">
        		<h2 dir="rtl">הודעה</h2>
        		<p dir="rtl">רוצה לתקן משהו? לחץ <a href="/#requestModal">  כאן </a>ומלא      את הטופס! </p>    
        		</div>
        	</div>
        	
        </div>
        
        <div id="requestModal" class="modal_req_Dialog">
            <div>
        		<a href="#close" title="Закрыть" class="close" style="
    opacity: 1.2;
">X</a>
        	<div class="white" style="padding: 20px; overflow: auto; height:509px; background-color: #ffffff;">
            
            
            <!------------FORM------------->
            
            <?php
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);

/**
 * Отмечает CheckBox и radiobutton.
 * 
 * @param mixed $var
 * @param string $value
 * @return string
 */
function checked($var, $value = null) 
{
	if (is_null($value)) {
		return ($var) ? ' checked' : '';
	} else {
		if (!is_array($var)) {
			$var = explode(',', $var);
		}
 
		return (in_array($value, $var)) ? ' checked' : '';
	}
}
$image_name = 'test';
$filePath = '../images/'.$image_name.'.jpg';

if (file_exists($filePath)) { 
    
    $pathsign = 'yes';
    }else{
        $pathsign = '';
    }


$content='

<html>
<head>
	<title>Contact V5</title>
		<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="form/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="form/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="form/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="form/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/noui/nouislider.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="form/css/main.css">
<!--===============================================================================================-->

</head>
<body>

	<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" method="post" action="../pdf/pdftest.php">
				<span class="contact100-form-title">
					טופס תיקון ציוד למעבדה גזר מחשבים
				</span>


            <div style="direction:rtl; padding-left:-50px;"> 

                <div class="wrap-input100 bg1 validate-input bg1" data-validate="Please Type Your Name">
                    <span class="label-input100"> שם מלא * (FULL NAME *)</span>
                    <input class="input100" type="text" name="name" placeholder="שם מלא">
                </div>

                <div class="wrap-input100 bg1 validate-input bg1" data-validate="Please Type Your Phone">
                    <span class="label-input100">טלפון* (Phone *)</span>
                    <input class="input100" type="text" name="phone" placeholder="טלפון">
                </div>
				
				
				<div class="w-full js-show-service">
					<div  class="wrap-input100 bg1">
						<span class="label-input100">פרטי ציוד לתיקון</span></div>
					</div>	
						

                        <div class="contact100-form-radio">
                            
                        </div>

						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio1" type="radio" name="type-product" value="1">
							<label class="label-radio100" for="radio1">
							מחשב	Computer
							</label>
						</div>

						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio2" type="radio" name="type-product" value="2">
							<label class="label-radio100" for="radio2">
							טלפון	Phone
							</label>
						</div>

						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio3" type="radio" name="type-product" value="3">
							<label  class="label-radio100" for="radio3">
							מצלמה	Camera
							</label>
						</div>
						
						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio4" type="radio" name="type-product" value="4">
							<label class="label-radio100" for="radio4">
							נגן	MP3
							</label>
						</div>	
						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio5" type="radio" name="type-product" value="5">
							<label class="label-radio100" for="radio5">
							אחר	other: 
							</label>
							<input class="input100" type="text" name="other_text" placeholder="Enter Your text ">
						</div>	
  					
				</div>
				
				<div class="wrap-input100 bg1 validate-input bg1" data-validate="Please Type model">
					<span class="label-input100">דגם Model</span>
					<input class="input100" type="text" name="model" placeholder="הזן את הדגם">
				</div>
				
				<div class="wrap-input100 bg1 validate-input bg1" data-validate="Please Type password">
					<span class="label-input100">סיסמה  Password</span>
					<input class="input100" type="text" name="password" placeholder="הזן את הסיסמה">
				</div>
				
				<div class="wrap-input100 bg1 validate-input bg1" data-validate="Asessories">
					<span class="label-input100">אביזרים Asessories</span>
					<input class="input100" type="text" name="asessories" placeholder="הזן את אביזרים  ">
				</div>
				
				

				<div class="wrap-input100 validate-input bg0 rs1-alert-validate" data-validate = "Please Type description">
					<span class="label-input100">Problem description</span>
					<textarea class="input100" name="desc" placeholder="ההודעה שלך כאן..."></textarea>
				</div>
				
				<div class="wrap-input100 bg1 validate-input bg1">
				    <p dir="rtl"><span class="font0" style="font-weight: bold;">תקנון מעבדת גזר מחשבים</span></p>
                		
                		<ul>
                		<li>
                			<p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;חובה על הלקוח לבצע גיבוי לפני מסירת המכשיר למעבדה. גזר מחשבים אינה אחראית על איבוד או&nbsp;מחיקת נתונים עקב תקלת תוכנה או חומרה.</span></p>
                		</li>
                		<li>
                			<p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;אין אחריות על תיקון קורוזיה ומים.</span></p>
                		</li>
                		<li>
                		<p dir="rtl"><span class="font0"></span></p>
                		<p dir="rtl"><span class="font0">
                		&bull; &nbsp;&nbsp; עלות בדיקה של מכשיר (לא נדלק, לא ) נטען, נזיקי מים וכו. </span></p>
                		</li>
                        <li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;אם יש צורך בהזמנת חלק התשלום יתבצע מראש.</span></p>
                		</li>
                		<li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;בדיקה או תיקון לוח מחשב, עלול לקחת זמן רב (כשבוע אחד).</span></p>
                		</li>
                		<li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;תיקונים מורכבים, עלולים לקחת לעתים זמן מה (כשבוע או יותר).</span></p>
                		</li>
                		<li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;אחריות על תיקון במעבדה הינה לשלושה חודשים. האחריות אינה כוללת נזקי תוכנה כגון וירוסים וכו&nbsp;גזר מחשבים אינה אחראית בכל צורה שהיא על נזקים קודמים במכשיר.</span></p>
                		</li>
                		<li>
                			<p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;תיקון ציוד באחריות מחייב הצגת חשבונית קניה מתאימה .</span></p>
                		</li>
                		<li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;מכשיר שלא ״אסף לאחר חודש יחשב כהפקר.</span></p>
                		</li>
                		</ul>
                </div>
                
                <div class="wrap-input100 bg1 validate-input bg1">
    				<p>
    				<li>
    				
    				<style>
    				    .sign_frame {
                              height: 300px;
                              width: 500px;
                          }
    				    @media screen and (max-width: 767px) {
                          .sign_frame {
                              height: 280px;
                              width: 220px;
                          }
                        }
    				</style>
    				
    				
    				
    					<iframe src="http://gezer.mysitik.com/form/sign" class="sign_frame" scrolling="no" style="overflow:hidden; margin-top:-4px; margin-left:-4px; border:none;">						
    				    </iframe>
    				</li>
    				</p>
				</div>
				
				<div class="wrap-input100 bg1 validate-input bg1" data-validate="Date">
					<span class="label-input100">תַאֲרִיך Date</span>
					<input class="input100" type="date" name="date">
				</div>

				<div class="container-contact100-form-btn">
					<button data-action="save-jpg" class="contact100-form-btn">
						<span>	
							שלח&nbsp;&nbsp;						     
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							
						</span>
					</button>
				</div>
				
			</form>
		</div>
	</div>
	</body>
</html>

';

echo $content;

?>

<script src="form/vendor/daterangepicker/moment.min.js"></script>
	<script src="form/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="form/vendor/countdowntime/countdowntime.js"></script>
<!--========================-->
	
<!--============================-->
	<script src="form/js/main.js"></script>
            
            
            <!----------ENDFORM------------>
            
				
					<!--<iframe class="form_frame" src="http://gezer.mysitik.com/form/" style="width: 100%;" scrolling="no" style="overflow:hidden; margin-top:-4px; margin-left:-4px; border:none;">						
				    </iframe>-->

<script>
    setTimeout(function(){
        window.location.href = "/#requestModal";
        
    },13000);
</script>

    </body>
</html>