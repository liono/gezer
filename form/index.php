<?php
ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);

/**
 * Отмечает CheckBox и radiobutton.
 * 
 * @param mixed $var
 * @param string $value
 * @return string
 */
function checked($var, $value = null) 
{
	if (is_null($value)) {
		return ($var) ? ' checked' : '';
	} else {
		if (!is_array($var)) {
			$var = explode(',', $var);
		}
 
		return (in_array($value, $var)) ? ' checked' : '';
	}
}
$image_name = 'test';
$filePath = '../images/'.$image_name.'.jpg';

if (file_exists($filePath)) { 
    
    $pathsign = 'yes';
    }else{
        $pathsign = '';
    }


$content='

<html dir="rtl" lang="en">
<head>
	<title>Contact V5</title>
		<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/noui/nouislider.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->

</head>
<body>

	<div class="container-contact100">
		<div class="wrap-contact100">
			<form class="contact100-form validate-form" method="post" action="../pdf/pdftest.php">
				<span class="contact100-form-title">
					טופס תיקון ציוד למעבדה גזר מחשבים
				</span>

				<div class="wrap-input100 validate-input bg1" data-validate="Please Type Your Name">
					<span class="label-input100"> שם מלא * (FULL NAME *)</span>
					<input class="input100" type="text" name="name" placeholder="שם מלא">
				</div>

				<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Enter Your Email (e@a.x)">
					<span class="label-input100">דואר אלקטרוני* (Email *)</span>
					<input class="input100" type="text" name="email" placeholder="דואר אלקטרוני ">
				</div>

				<div class="wrap-input100 bg1 rs1-wrap-input100 validate-input bg1" data-validate="Please Type Your Phone">
					<span class="label-input100">טלפון* Phone</span>
					<input class="input100" type="text" name="phone" placeholder="טלפון">
				</div>
				
				
				<div class="w-full js-show-service">
					<div class="wrap-contact100-form-radio">
						<span class="label-input400">פרטי ציוד לתיקון</span>
						
						

						<div class="contact100-form-radio m-t-15">
							<input class="input-radio100" id="radio1" type="radio" name="type-product" value="1">
							<label class="label-radio100" for="radio1">
							מחשב	Computer
							</label>
						</div>

						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio2" type="radio" name="type-product" value="2">
							<label class="label-radio100" for="radio2">
							טלפון	Phone
							</label>
						</div>

						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio3" type="radio" name="type-product" value="3">
							<label class="label-radio100" for="radio3">
							מצלמה	Camera
							</label>
						</div>
						
						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio4" type="radio" name="type-product" value="4">
							<label class="label-radio100" for="radio4">
							נגן	MP3
							</label>
						</div>	
						<div class="contact100-form-radio">
							<input class="input-radio100" id="radio5" type="radio" name="type-product" value="5">
							<label class="label-radio100" for="radio5">
							אחר	other: 
							</label>
							<input class="input100" type="text" name="other_text" placeholder="Enter Your text ">
						</div>	
  					</div>
				</div>
				
				<div class="wrap-input100 bg1 rs1-wrap-input100 validate-input bg1" data-validate="Please Type model">
					<span class="label-input100">דגם Model</span>
					<input class="input100" type="text" name="model" placeholder="הזן את הדגם">
				</div>
				
				<div class="wrap-input100 bg1 rs1-wrap-input100 validate-input bg1" data-validate="Please Type password">
					<span class="label-input100">סיסמה  Password</span>
					<input class="input100" type="text" name="password" placeholder="הזן את הסיסמה">
				</div>
				
				<div class="wrap-input100 bg1 rs1-wrap-input100 validate-input bg1" data-validate="Asessories">
					<span class="label-input100">אביזרים Asessories</span>
					<input class="input100" type="text" name="asessories" placeholder="הזן את אביזרים  ">
				</div>
				
				<div class="wrap-input100 bg1 rs1-wrap-input100 validate-input bg1" data-validate="מחיר משוער  Authorized price">
					<span class="label-input100">Authorized price</span>
					<input class="input100" type="text" name="authprice" placeholder=" הזן את מחיר משוער ">
				</div>				

				<div class="wrap-input100 validate-input bg0 rs1-alert-validate" data-validate = "Please Type description">
					<span class="label-input100">Problem description</span>
					<textarea class="input100" name="desc" placeholder="ההודעה שלך כאן..."></textarea>
				</div>
				
				<div class="wrap-input100 bg1 validate-input bg1">
				    <p dir="rtl"><span class="font0" style="font-weight: bold;">תקנון מעבדת גזר מחשבים</span></p>
                		
                		<ul>
                		<li>
                			<p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;חובה על הלקוח לבצע גיבוי לפני מסירת המכשיר למעבדה. גזר מחשבים אינה אחראית על איבוד או&nbsp;מחיקת נתונים עקב תקלת תוכנה או חומרה.</span></p>
                		</li>
                		<li>
                			<p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;תיקון ציוד באחריות מחייב הצגת חשבונית קניה מתאימה .</span></p>
                		</li>
                		<li>
                			<p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;אין אחריות על תיקון קורוזיה ומים.</span></p>
                		</li>
                		<li>
                		<p dir="rtl"><span class="font0"></span></p>
                		<p dir="rtl"><span class="font0">
                		&bull; עלות כניסה ₪30 </span></p>
                		</li>
                        <li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;אם יש צורך בהזמנת חלק התשלום יתבצע מראש.</span></p>
                		</li>
                		<li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;בדיקה או תיקון לוח מחשב, עלול לקחת זמן רב (כשבוע אחד).</span></p>
                		</li>
                		<li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;תיקונים מורכבים, עלולים לקחת לעתים זמן מה (כשבוע או יותר).</span></p>
                		</li>
                		<li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;תיקון מעל סכום של מחיר משוער יבוצע לאחר אישור טלפוני מהלקוח, עד סכום זה תתוקן התקלה&nbsp;ללא אישור.</span></p>
                		</li>
                		<li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;אחריות על תיקון במעבדה הינה לשלושה חודשים. האחריות אינה כוללת נזקי תוכנה כגון וירוסים וכו&nbsp;גזר מחשבים אינה אחראית בכל צורה שהיא על נזקים קודמים במכשיר.</span></p>
                		</li>
                		<li>
                		    <p dir="rtl"><span class="font0">&bull; &nbsp;&nbsp;&nbsp;מכשיר שלא ״אסף לאחר חודש יחשב כהפקר.</span></p>
                		</li>
                </div>
                
                <div class="wrap-input100 bg1 validate-input bg1">
    				<p>
    				<li>
    				
    				<style>
    				    .sign_frame {
                              height: 300px;
                              width: 500px;
                          }
    				    @media screen and (max-width: 767px) {
                          .sign_frame {
                              height: 280px;
                              width: 220px;
                          }
                        }
    				</style>
    				
    				
    				
    					<iframe src="http://gezer.mysitik.com/form/sign" class="sign_frame" scrolling="no" style="overflow:hidden; margin-top:-4px; margin-left:-4px; border:none;">						
    				    </iframe>
    				</li>
    				</p>
				</div>
				
				<div class="wrap-input100 bg1 validate-input bg1" data-validate="Date">
					<span class="label-input100">תַאֲרִיך Date</span>
					<input class="input100" type="date" name="date" placeholder="הזן את date">
				</div>

				<div class="container-contact100-form-btn">
					<button data-action="save-jpg" class="contact100-form-btn">
						<span>	
							שלח&nbsp;&nbsp;						     
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
							
						</span>
					</button>
				</div>
				
			</form>
		</div>
	</div>
	</body>
</html>

';

echo $content;

?>








<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>



<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});


			$(".js-select2").each(function(){
				$(this).on('select2:close', function (e){
					if($(this).val() == "Please chooses") {
						$('.js-show-service').slideUp();
					}
					else {
						$('.js-show-service').slideUp();
						$('.js-show-service').slideDown();
					}
				});
			});
		})
	</script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--========================-->
	
<!--============================-->
	<script src="js/main.js"></script>



<!--==================================================-->
	<script src="js/main.js"></script>






