<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php

/**
 * Отмечает CheckBox и radiobutton.
 * 
 * @param mixed $var
 * @param string $value
 * @return string
 */
function checked($var, $value = null) 
{
	if (is_null($value)) {
		return ($var) ? ' checked' : '';
	} else {
		if (!is_array($var)) {
			$var = explode(',', $var);
		}
 
		return (in_array($value, $var)) ? ' checked' : '';
	}
}

$content='<html dir="rtl" lang="he" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Untitled Form</title>
<link rel="stylesheet" type="text/css" href="view.css" media="all">
<script type="text/javascript" src="view.js"></script>
<script type="text/javascript" src="calendar.js"></script>
</head>
<body>
	

	<div id="form_container">
	
		<h1><a>Untitled Form</a></h1>
		<form id="form_111450" class="appnitro"  method="post" action="./pdftest.php">
					<div class="form_description">
			<h2>Untitled Form</h2>
			<p>This is your form description. Click here to edit.</p>
		</div>						
			<ul>
			
					<li id="li_1" >
		<label class="description" for="element_1">שם מלא (Full name)</label>
		<div>
			   <input id="element_1" name="element_1" class="element text medium" id="element_1" type="text" maxlength="255"/>
		</div> 
		</li>		<li id="li_2" >
		<label class="description" for="element_2">טלפון (Phone) </label>
		<span>
			<input id="element_2_1" name="element_2_1" class="element text" size="3" maxlength="3" value="" type="text"> -
			<label for="element_2_1">(###)</label>
		</span>
		<span>
			<input id="element_2_2" name="element_2_2" class="element text" size="3" maxlength="3" value="" type="text"> -
			<label for="element_2_2">###</label>
		</span>
		<span>
	 		<input id="element_2_3" name="element_2_3" class="element text" size="4" maxlength="4" value="" type="text">
			<label for="element_2_3">####</label>
		</span>
		 
		</li>		
		<li id="li_3" >
		<label class="description" for="element_3">Multiple Choice </label>
		<div>
		<span>
			<input id="element_3" name="element_3" class="element radio" type="radio" value="1" <?php echo checked($element_3, 1); ?> />
			<label class="choice" for="element_3_1">Computer</label>
		</span>
		</div>
<div>
<span>
<input id="element_3_2" name="element_3" class="element radio" type="radio" value="2" />
<label class="choice" for="element_3_2">Phone</label>
</span>
</div>
<div>
<span>
<input id="element_3_3" name="element_3" class="element radio" type="radio" value="3" />
<label class="choice" for="element_3_3">Camera</label>
</span>
</div>
<div>
<span>
<input id="element_3_4" name="element_3" class="element radio" type="radio" value="4" />
<label class="choice" for="element_3_4">MP3</label>
</span>
</div>
<div>
<span>
<input id="element_3_5" name="element_3" class="element radio" type="radio" value="5" />
<label class="choice" for="element_3_5">Other</label>
		</span> 
		</li></div>
		
		<li id="li_4" >
		<label class="description" for="element_4">Model </label>
		<div>
			<input id="element_4" name="element_4" class="element text medium" type="text" maxlength="255" value=""/> 
		</div> 
		</li>		<li id="li_5" >
		<label class="description" for="element_5">Password </label>
		<div>
			<input id="element_5" name="element_5" class="element text medium" type="text" maxlength="255" value=""/> 
		</div> 
		</li>		<li id="li_6" >
		<label class="description" for="element_6">Asessories </label>
		<div>
			<input id="element_6" name="element_6" class="element text medium" type="text" maxlength="255" value=""/> 
		</div> 
		</li>		<li id="li_7" >
		<label class="description" for="element_7">Authorized price </label>
		<div>
			<input id="element_7" name="element_7" class="element text medium" type="text" maxlength="255" value=""/> 
		</div> 
		</li>		<li id="li_8" >
		<label class="description" for="element_8">Finel price  </label>
		<div>
			<input id="element_8" name="element_8" class="element text medium" type="text" maxlength="255" value=""/> 
		</div> 
		</li>		<li id="li_9" >
		<label class="description" for="element_9">Problem description </label>
		<div>
			<input id="element_9" name="element_9" class="element text medium" type="text" maxlength="255" value=""/> 
		</div> 
		</li>		<li id="li_10" >
		<label class="description" for="element_10">Signature </label>
		<div>
			<input id="element_10" name="element_10" class="element text medium" type="text" maxlength="255" value=""/> 
		</div> 
		</li>		<li id="li_11" >
		<label class="description" for="element_11">Date </label>
		<span>
			<input id="element_11_1" name="element_11_1" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_11_1">MM</label>
		</span>
		<span>
			<input id="element_11_2" name="element_11_2" class="element text" size="2" maxlength="2" value="" type="text"> /
			<label for="element_11_2">DD</label>
		</span>
		<span>
	 		<input id="element_11_3" name="element_11_3" class="element text" size="4" maxlength="4" value="" type="text">
			<label for="element_11_3">YYYY</label>
		</span>
	
		<span id="calendar_11">
			<img id="cal_img_11" class="datepicker" src="calendar.gif" alt="Pick a date.">	
		</span>
		<script type="text/javascript">
			Calendar.setup({
			inputField	 : "element_11_3",
			baseField    : "element_11",
			displayArea  : "calendar_11",
			button		 : "cal_img_11",
			ifFormat	 : "%B %e, %Y",
			onSelect	 : selectDate
			});
		</script>
		 
		</li>
			
					<li class="buttons">
			    <input type="hidden" name="form_id" value="111450" />
			    
				<input id="saveForm" class="button_text" type="submit" name="submit" value="Submit" />
		</li>
			</ul>
		</form>	
		
	</div>
	
	</body>
</html>';
//echo $_POST["element_1"];
echo $content;

ini_set('display_errors', 1); 
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);
require_once __DIR__ . '/../vendor/autoload.php';
$mpdf = new \Mpdf\Mpdf(); 
//$mpdf->WriteHTML($content); 
//$mpdf->Output();

?>